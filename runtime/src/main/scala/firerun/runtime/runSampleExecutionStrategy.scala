package firerun.runtime

import java.nio.file.Paths

import firelib.common.config.ModelBacktestConfig
import firelib.common.core.BacktestMode
import firelib.execution.ModelExecutionLauncher
import firelib.execution.config.{ModelExecutionConfig, TradeGateConfig}
import firelib.ibadapter.IbTradeGate
import firelib.spydia.runSampleBacktest

/**
 * to run strats from IDE
 */
object runSampleExecutionStrategy {

    def main(args: Array[String]): Unit = {

        val backtestConfig: ModelBacktestConfig = runSampleBacktest.createSampleBacktestConfig

        backtestConfig.reportTargetPath = Paths.get(backtestConfig.reportTargetPath + "Exec").toAbsolutePath.toString
        backtestConfig.backtestMode = BacktestMode.SimpleRun

        backtestConfig.modelParams("trading.hour") = "18"

        val cfg = new ModelExecutionConfig()

        cfg.backtestConfig = backtestConfig

        confIbGateway(cfg)

        //running backtest before launching to initialize quantile
        cfg.runBacktestBeforeStrategyRun = true
        val container: ModelExecutionLauncher = new ModelExecutionLauncher(cfg)
        container.start()
    }

    def confIbGateway(cfg: ModelExecutionConfig) {
        cfg.tradeGateConfig = new TradeGateConfig
        cfg.tradeGateConfig.gateClassName = classOf[IbTradeGate].getName
        cfg.tradeGateConfig.gateParams = Map("port" -> "4001", "client.id" -> "1")
        cfg.marketDataProviderConfig = null
    }

}
