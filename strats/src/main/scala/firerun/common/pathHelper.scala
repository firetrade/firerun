package firerun.common

import java.nio.file.{Paths, Path}
import java.util.Properties


object pathHelper {

    private val props = loadProperties("/main.properties")

    def dsRoot : Path ={
        Paths.get(props.getProperty("data.server.root"))
    }

    def reportRoot : Path ={
        Paths.get(props.getProperty("report.root"))
    }

    def loadProperties(propFile : String) : Properties = {
        val ret = new Properties()
        ret.load(getClass().getResourceAsStream(propFile))
        ret
    }


}
